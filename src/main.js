import Vue from 'vue'
import App from './App.vue'

import('@/assets/styles/index.css');

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'


                       
Vue.config.productionTip = false

Vue.use(VueMaterial)

new Vue({
  render: h => h(App),
}).$mount('#app')
